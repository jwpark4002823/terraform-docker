variable "mysql_root_password" {
  type        = string
  description = "root password"
  sensitive   = true
}

variable "mysql_user_name" {
  type        = string
  description = "user name"
}

variable "mysql_database_name" {
  type        = string
  description = "database name"
}

variable "docker_network" {
  type        = string
  description = "docker network"
}
