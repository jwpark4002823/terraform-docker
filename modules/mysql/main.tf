terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.2"
    }
  }
}

resource "docker_image" "mysql" {
  name         = "mysql:8.0"
  keep_locally = true
}

resource "docker_container" "mysql" {
  name  = "dev-mysql"
  image = docker_image.mysql.image_id

  env = [
    "MYSQL_ROOT_PASSWORD=${var.mysql_root_password}",
    "MYSQL_DATABASE=${var.mysql_database_name}",
    "MYSQL_USER=${var.mysql_user_name}",
    "MYSQL_PASSWORD=${var.mysql_root_password}"
  ]

  networks_advanced {
    name = var.docker_network
  }

  volumes {
    container_path = "/var/lib/mysql"
    host_path      = "/root/rcode/terras/data/mysql"
  }

  ports {
    internal = 3306
    external = 3306
  }
}
