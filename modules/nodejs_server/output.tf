output "ip_address" {
  value = docker_container.nodejs_server.network_data[0].ip_address
}
