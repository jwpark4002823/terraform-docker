terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.2"
    }
  }
}

resource "docker_image" "nodejs_server" {
  name         = "my-node-app:0.0.4"
  keep_locally = true
}

resource "docker_container" "nodejs_server" {
  name  = var.server_name
  image = docker_image.nodejs_server.image_id

  env = [
    "DB_HOST=${var.db_host}",
    "DB_USER=${var.db_user}",
    "DB_PASSWORD=${var.db_password}",
    "DB_DATABASE=${var.db_database}",
    "PORT=${var.port}"
  ]

  networks_advanced {
    name = var.docker_network
  }

  ports {
    internal = var.port
    external = var.port
  }
}
