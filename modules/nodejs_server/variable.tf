variable "db_host" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_password" {
  type      = string
  sensitive = true
}

variable "db_database" {
  type = string
}

variable "port" {
  type = number
}

variable "server_name" {
  type = string
}

variable "docker_network" {
  type = string
}
