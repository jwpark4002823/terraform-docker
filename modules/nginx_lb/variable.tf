variable "server1_ip" {
  type = string
}

variable "server2_ip" {
  type = string
}

variable "docker_network" {
  type = string
}
