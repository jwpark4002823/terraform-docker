terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.2"
    }
  }
}

resource "docker_image" "nginx" {
  name         = "nginx:1.25.0"
  keep_locally = true
}

resource "docker_container" "nginx" {
  name  = "nginx_lb"
  image = docker_image.nginx.image_id

  env = [
    "SERVER1_IP=${var.server1_ip}",
    "SERVER2_IP=${var.server2_ip}"
  ]

  networks_advanced {
    name = var.docker_network
  }

  volumes {
    container_path = "/etc/nginx/templates"
    host_path      = "/root/rcode/terras/data/nginx/template"
  }

  ports {
    internal = 80
    external = 8000
  }
}
