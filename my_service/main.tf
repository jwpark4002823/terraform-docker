terraform {
  required_version = "~> 1.4.0"

  # https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.2"
    }
  }

  cloud {
    organization = "melius-devstack"
    hostname     = "app.terraform.io"

    workspaces {
      name = "webserver-in-docker"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

locals {
  server1_port  = 8081
  server2_port  = 8082
  database_name = "terras"
}

resource "docker_network" "my_network" {
  name = "my_network"
}

module "mysql" {
  source              = "../modules/mysql"
  docker_network      = docker_network.my_network.id
  mysql_root_password = var.db_password
  mysql_user_name     = var.db_user
  mysql_database_name = local.database_name
}

module "nodejs_server" {
  for_each = {
    server1 = {
      server_name = "nodejs_server_8081"
      port        = local.server1_port
    }
    server2 = {
      server_name = "nodejs_server_8082"
      port        = local.server2_port
    }
  }
  source         = "../modules/nodejs_server"
  docker_network = docker_network.my_network.id
  server_name    = each.value.server_name
  db_host        = module.mysql.ip_address
  db_user        = var.db_user
  db_password    = var.db_password
  db_database    = local.database_name
  port           = each.value.port
}

module "nginx_lb" {
  source         = "../modules/nginx_lb"
  docker_network = docker_network.my_network.id
  server1_ip     = "${module.nodejs_server["server1"].ip_address}:${local.server1_port}"
  server2_ip     = "${module.nodejs_server["server2"].ip_address}:${local.server2_port}"
}
