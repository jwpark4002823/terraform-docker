variable "db_password" {
  type        = string
  description = "db password"
  sensitive   = true
}

variable "db_user" {
  type        = string
  description = "db name"
}
